//
//  ViewController.swift
//  Home task 6
//
//  Created by Александр Зарудний on 10/17/21.
//

import UIKit
import SnapKit
import MapKit
import CoreLocation
import Combine

class ViewController: UIViewController {
    private var isFirstTime = true
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.delegate = self
        return mapView
    }()
    
    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    var observers: [AnyCancellable] = []
    private var arrayAnnotations = [MKPointAnnotation]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        mapView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.height.equalToSuperview()
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        let viewModel = VMBankEntities()
        
        viewModel.mutableAnnotations.sink { value in
            self.arrayAnnotations = viewModel.sortLocationsBankEntities(arrayPoints: value, locationManager: self.locationManager)
            self.checkLocationAuthorization()
            print(self.arrayAnnotations.count)
            self.setPins(arrayAnnotations: self.arrayAnnotations)
        }.store(in: &observers)
    }
    
    //MARK: ~set map center
    func checkLocationAuthorization() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            mapView.showsUserLocation = true
            followUserLocation()
            break
        case .denied, .notDetermined, .restricted:
            let location = CLLocationCoordinate2D(latitude: 52.431233, longitude: 30.992697)
            let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
            break
        default:
            break
        }
        
        locationManager.startUpdatingLocation()
    }
    
    //MARK: ~set map on user location
    func followUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
            self.setPins(arrayAnnotations: self.arrayAnnotations)
            mapView.setRegion(region, animated: true)
        }
    }
    
    //MARK: ~set pins
    func setPins(arrayAnnotations: [MKPointAnnotation]) {
        
        DispatchQueue.main.async {
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotations(arrayAnnotations)
        }
    }
    
}

//MARK: ~location manager, mapView delegate
extension ViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isFirstTime {
            isFirstTime = false
            guard let location = locations.first else { return }
            let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: 4000, longitudinalMeters: 4000)
            mapView.setRegion(region, animated: true)
        }
        if let location = manager.location {
            self.userLocation = location
        }
    }
}

