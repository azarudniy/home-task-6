//
//  BankEntity.swift
//  Home task 6
//
//  Created by Александр Зарудний on 10/18/21.
//

import Foundation

struct BankEntity: BankEntityProtocol {
    var gps_x: String
    var gps_y: String
    
    enum CodingKeys1: String, CodingKey {
        case gps_x
        case gps_y
    }
    
    enum CodingKeys2: String, CodingKey {
        case gps_x = "GPS_X"
        case gps_y = "GPS_Y"
    }
    
    init(from decoder: Decoder) throws{
        do {
            let container = try decoder.container(keyedBy: CodingKeys1.self)
            try self.init(container)
        } catch  {
            let container = try decoder.container(keyedBy: CodingKeys2.self)
            try self.init(container)
        }
    }
    
    private init (_ container: KeyedDecodingContainer<CodingKeys1>) throws {
        gps_x = try container.decode(String.self, forKey: .gps_x)
        gps_y = try container.decode(String.self, forKey: .gps_y)
    }
    
    private init (_ container: KeyedDecodingContainer<CodingKeys2>) throws {
        gps_x = try container.decode(String.self, forKey: .gps_x)
        gps_y = try container.decode(String.self, forKey: .gps_y)
    }}
