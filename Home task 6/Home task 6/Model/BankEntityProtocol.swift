//
//  BankEntityProtocol.swift
//  Home task 6
//
//  Created by Александр Зарудний on 10/17/21.
//

import Foundation

protocol BankEntityProtocol: Codable {
    var gps_x: String { get set }
    var gps_y: String { get set }
}
