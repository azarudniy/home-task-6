//
//  VMBankEntities.swift
//  Home task 6
//
//  Created by Александр Зарудний on 10/18/21.
//

import Foundation
import Combine
import MapKit

class VMBankEntities {
    private let urlStringATM = "https://belarusbank.by/api/atm?city=Гомель"
    private let urlStringTerminals = "https://belarusbank.by/api/infobox?city=Гомель"
    private let urlStringBankOffices = "https://belarusbank.by/api/filials_info?city=Гомель"
    
    private var arrayURL: [URL] = [URL]()
    
    var arrayAnnotations = [MKPointAnnotation]() {
        didSet {
            mutableAnnotations.send(arrayAnnotations)
        }
    }
    
    let mutableAnnotations = PassthroughSubject<[MKPointAnnotation], Never>()
    
    
    required init() {
        if let urlStringATMEncoded = urlStringATM.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlStringTerminalsEncoded = urlStringTerminals.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlStringBankOfficesEncoded = urlStringBankOffices.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlATM = URL(string: urlStringATMEncoded),
           let urlTerminals = URL(string: urlStringTerminalsEncoded),
           let urlBankOffices = URL(string: urlStringBankOfficesEncoded) {
            arrayURL.append(urlATM)
            arrayURL.append(urlTerminals)
            arrayURL.append(urlBankOffices)
        }
        self.syncParsingData(arrayURL: arrayURL)
    }
    
    private func syncParsingData(arrayURL: [URL]) {
        let queue = DispatchQueue(label: "serial queue")
        for url in arrayURL {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    return
                }
                queue.async {
                    do {
                        if let data = data {
                            let arrayBankEntities = try JSONDecoder().decode([BankEntity].self, from: data)
                            var arrayPoints = [MKPointAnnotation]()
                            for bankEntity in arrayBankEntities {
                                let annotation = MKPointAnnotation()
                                if let latitude = Double(bankEntity.gps_x),
                                   let longitude = Double(bankEntity.gps_y) {
                                    annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                                    arrayPoints.append(annotation)
                                }
                            }
                            self.arrayAnnotations.append(contentsOf: arrayPoints)
                        }
                    } catch {
                        return
                    }
                }
            }.resume()
        }
    }
    
    func sortLocationsBankEntities(arrayPoints: [MKPointAnnotation], locationManager: CLLocationManager) -> [MKPointAnnotation] {
        if arrayPoints.count < 10 {
            return arrayPoints
        }
        return Array(arrayPoints.sorted {
            self.getDistance(point: $0, locationManager: locationManager) < self.getDistance(point: $1, locationManager: locationManager)
        }[0...9])
    }
    
    private func getDistance(point: MKPointAnnotation, locationManager: CLLocationManager) -> Double {
        let location = CLLocation(latitude: point.coordinate.latitude, longitude: point.coordinate.longitude)
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
            case .authorizedAlways, .authorizedWhenInUse:
                guard let userLocation = CLLocationManager().location else { return 0 }
                return location.distance(from: userLocation)
            case .denied, .notDetermined, .restricted:
                return location.distance(from: CLLocation(latitude: 52.431233, longitude: 30.992697))
            default:
                return 0
            }
        } else {
            return location.distance(from: CLLocation(latitude: 52.431233, longitude: 30.992697))
        }
    }
}

